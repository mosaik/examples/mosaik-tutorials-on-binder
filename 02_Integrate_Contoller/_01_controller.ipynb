{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "f5c23fd5-483e-4838-89b0-320138f74901",
   "metadata": {},
   "source": [
    "## Adding a control mechanism to a scenario"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "63bc70a9-95de-4864-b142-7fb2b0f76b49",
   "metadata": {},
   "source": [
    "Now that we integrated our first simulator into mosaik and tested it in a simple scenario, we should implement a control mechanism and mess around with our example simulator a little bit."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0d1c3ec7-a87f-4455-88e5-82d6465c0583",
   "metadata": {},
   "source": [
    "As you remember, our example models had a value to which they added something in each step. Eventually, their value will end up being very high. We’ll use a multi-agent system to keep the values of our models in [-3, 3]. The agents will monitor the current value of their respective models and when it reaches -3/3, they will set delta to 1/-1 for their model."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bc5998ba-2d13-4ff3-904a-d1fcfabe99f8",
   "metadata": {},
   "source": [
    "Implementing the Sim API for control strategies is very similar to implementing it for normal simulators. We start again by importing the `mosaik_api` package and defining the simulator meta data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "d342ee4d-4069-496d-b678-7bd30895c04c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# controller.py\n",
    "\"\"\"\n",
    "A simple demo controller.\n",
    "\n",
    "\"\"\"\n",
    "import mosaik_api\n",
    "\n",
    "\n",
    "META = {\n",
    "    'type': 'event-based',\n",
    "    'models': {\n",
    "        'Agent': {\n",
    "            'public': True,\n",
    "            'params': [],\n",
    "            'attrs': ['val_in', 'delta'],\n",
    "        },\n",
    "    },\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "437f608e-685d-49ba-b6da-3b578db915a7",
   "metadata": {},
   "source": [
    "We set the `type` of the simulator to ‘event-based’. As we have learned, this has two main implications:\n",
    "\n",
    "1. Whenever another simulator provides new input for the simulator, a step is triggered (at the output time). So we don’t need to take care of the synchronisation of the models and agents. As our example simulator is of type time-based, it is only stepped at its self-defined times and will thus not be triggered by (potential) outputs of the agents. It will receive any output of the agents in its subsequent step.\n",
    "\n",
    "2. The provision of output of event-based simulators is optional. So if there’s nothing to report at a specific step, the attributes can (and should be) omitted in the get_data’s return dictionary.\n",
    "\n",
    "Our control mechanism will use agents to control other entities. The agent has no parameters and two attributes, the input ‘val_in’ and the output ‘delta’.\n",
    "\n",
    "Let’s continue and implement `mosaik_api.Simulator`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "08fb02aa-226d-4c20-b1ea-9779f6e6bf20",
   "metadata": {},
   "outputs": [],
   "source": [
    "class Controller(mosaik_api.Simulator):\n",
    "    def __init__(self):\n",
    "        super().__init__(META)\n",
    "        self.agents = []\n",
    "        self.data = {}\n",
    "        self.time = 0"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bd25e7e7-3ff4-4fb4-86e4-bc974811321a",
   "metadata": {},
   "source": [
    "Again, nothing special is going on here. We pass our meta data dictionary to our super class and set an empty list for our agents.\n",
    "\n",
    "Because our agents don’t have an internal concept of time, we don’t need to take care of the time_resolution of the scenario. And as there aren’t any simulator parameters either, we don’t need to implement `init()`. The default implementation will return the meta data, so there’s nothing we need to do in this case.\n",
    "\n",
    "Implementing `create()` is also straight forward:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "bf872c5d-8b60-42e6-9e1e-3c900e438a00",
   "metadata": {},
   "outputs": [],
   "source": [
    "def create(self, num, model):\n",
    "        n_agents = len(self.agents)\n",
    "        entities = []\n",
    "        for i in range(n_agents, n_agents + num):\n",
    "            eid = 'Agent_%d' % i\n",
    "            self.agents.append(eid)\n",
    "            entities.append({'eid': eid, 'type': model})\n",
    "\n",
    "        return entities"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "30664d4a-3c2b-4e01-bdc2-f4ca54fd8807",
   "metadata": {},
   "source": [
    "Every agent gets an ID like `“Agent_*<num>*”`. Because there might be multiple `create()` calls, we need to keep track of how many agents we already created in order to generate correct entity IDs. We also create a list of {‘eid’: ‘Agent_<num>’, ‘type’: ‘Agent’} dictionaries for mosaik.\n",
    "\n",
    "You may have noticed that we, in contrast to our example simulator, did not actually instantiate any real simulation models this time. We just pretend to do it. This okay, since we’ll implement the agent’s “intelligence” directly in `step()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "b5bbc82d-63da-4ba1-8372-9eba27ac07c5",
   "metadata": {},
   "outputs": [],
   "source": [
    "def step(self, time, inputs, max_advance):\n",
    "        self.time = time\n",
    "        data = {}\n",
    "        for agent_eid, attrs in inputs.items():\n",
    "            delta_dict = attrs.get('delta', {})\n",
    "            if len(delta_dict) > 0:\n",
    "                data[agent_eid] = {'delta': list(delta_dict.values())[0]}\n",
    "                continue\n",
    "\n",
    "            values_dict = attrs.get('val_in', {})\n",
    "            if len(values_dict) != 1:\n",
    "                raise RuntimeError('Only one ingoing connection allowed per '\n",
    "                                   'agent, but \"%s\" has %i.'\n",
    "                                   % (agent_eid, len(values_dict)))\n",
    "            value = list(values_dict.values())[0]\n",
    "            if value >= 3:\n",
    "                delta = -1\n",
    "            elif value <= -3:\n",
    "                delta = 1\n",
    "            else:\n",
    "                continue\n",
    "                \n",
    "            data[agent_eid] = {'delta': delta}\n",
    "            \n",
    "        self.data = data\n",
    "        \n",
    "        return None"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dc6c4a09-f488-4645-badf-bfae0e0bf3bb",
   "metadata": {},
   "source": [
    "The `inputs` arguments is a nested dictionary and will look like this:"
   ]
  },
  {
   "cell_type": "raw",
   "id": "e21a1f35-e3da-4b8f-9190-e8c4e2fca9d9",
   "metadata": {},
   "source": [
    "{\n",
    "    'Agent_0': {'val_in': {'ExampleSim-0.Model_0': -1}},\n",
    "    'Agent_1': {'val_in': {'ExampleSim-0.Model_1': 1}},\n",
    "    'Agent_2': {'val_in': {'ExampleSim-0.Model_2': 3}}\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a2891c13-29f3-40dc-8f99-de7b92be0084",
   "metadata": {},
   "source": [
    "For each agent, there’s a dictionary with all input attributes (in this case only ‘val_in’), containing the source entities (their full_id) with the corresponding values as key-value pairs.\n",
    "\n",
    "First we initialize an empty `data` dict that will contain the set-points that our control mechanism is creating for the models of the example simulator. We’ll fill this dict in the following loop. We iterate over all agents and extract its input ‘val_in’; so `values_dict` is a dict containing the current values of all models connected to that agent. In our example we only allow to connect one model per agent, and fetch its value.\n",
    "\n",
    "We now do the actual check:"
   ]
  },
  {
   "cell_type": "raw",
   "id": "428d86be-77c2-4c56-aaea-f5cbc943fac6",
   "metadata": {},
   "source": [
    "            if value >= 3:\n",
    "                delta = -1\n",
    "            elif value <= -3:\n",
    "                delta = 1\n",
    "            else:\n",
    "                continue"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3e6e04f3-1c32-4379-811d-07be58ba254e",
   "metadata": {},
   "source": [
    "If the value is ≤ -3 or ≥ 3, we have to set a new delta value. Else, we don’t need to do anything and can continue with a new iteration of the loop.\n",
    "\n",
    "If we have a new delta, we add it to the `data` dict:"
   ]
  },
  {
   "cell_type": "raw",
   "id": "404a4e75-41a1-439d-a93a-9b7148354ae6",
   "metadata": {},
   "source": [
    "            data[agent_eid] = {'delta': delta}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "04a942cd-475b-4070-b8e4-60541d2c1e8a",
   "metadata": {},
   "source": [
    "After finishing the loop, the `data` dict may look like this:"
   ]
  },
  {
   "cell_type": "raw",
   "id": "f38ba5d9-c392-40d0-9427-e9bf5e37dc41",
   "metadata": {},
   "source": [
    "{\n",
    "    'Agent_0': {'delta': 1},\n",
    "    'Agent_2': {'delta': -1},\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f8172781-47bd-4469-825f-15caa60437ab",
   "metadata": {},
   "source": [
    "Agent_0 sets the new delta = 1, and Agent_2 sets the new delta = -1. Agent_1 did not set a new delta.\n",
    "\n",
    "At the end of the step, we put the data dict to the class attribute self.data, to make it accessible in the get_data method"
   ]
  },
  {
   "cell_type": "raw",
   "id": "47001e20-4a9c-4172-8c6f-22921a8f0032",
   "metadata": {},
   "source": [
    "        self.data = data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aa97369e-56f5-463b-84b8-0580ca1bcf49",
   "metadata": {},
   "source": [
    "We return *None* to mosaik, as we don’t want to step ourself, but only when the controlled models provide new values."
   ]
  },
  {
   "cell_type": "raw",
   "id": "ddbc53c8-a09e-4553-8b1c-d7f16a655c4f",
   "metadata": {},
   "source": [
    "        return None"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e7c81e80-28bd-4e2d-9d2e-e30d0d1ab28c",
   "metadata": {},
   "source": [
    "After having called step, mosaik requests the new set-points via the get_data function. In principle we could just return the self.data dictionary, as we already constructed that in the adequate format. For illustrative purposes we do it manually anyhow. Additionally, if we do it like that, we can only send back the attributes that are actually needed by (connected to) other simulators:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "86a35ce0-d164-48c2-b43f-97082cc36af1",
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_data(self, outputs):\n",
    "        data = {}\n",
    "        for agent_eid, attrs in outputs.items():\n",
    "            for attr in attrs:\n",
    "                if attr != 'delta':\n",
    "                    raise ValueError('Unknown output attribute \"%s\"' % attr)\n",
    "                if agent_eid in self.data:\n",
    "                    data['time'] = self.time\n",
    "                    data.setdefault(agent_eid, {})[attr] = self.data[agent_eid][attr]\n",
    "\n",
    "        return data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "321b26ad-0e46-4075-bd4b-03264dc2a925",
   "metadata": {},
   "source": [
    "Here is the complete code for our (very simple) controller / mutli-agent system:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "f346733f-d7de-487e-979a-c0a2bcaf8010",
   "metadata": {},
   "outputs": [],
   "source": [
    "# controller.py\n",
    "\"\"\"\n",
    "A simple demo controller.\n",
    "\n",
    "\"\"\"\n",
    "import mosaik_api\n",
    "\n",
    "\n",
    "META = {\n",
    "    'type': 'event-based',\n",
    "    'models': {\n",
    "        'Agent': {\n",
    "            'public': True,\n",
    "            'params': [],\n",
    "            'attrs': ['val_in', 'delta'],\n",
    "        },\n",
    "    },\n",
    "}\n",
    "\n",
    "\n",
    "class Controller(mosaik_api.Simulator):\n",
    "    def __init__(self):\n",
    "        super().__init__(META)\n",
    "        self.agents = []\n",
    "        self.data = {}\n",
    "        self.time = 0\n",
    "\n",
    "    def create(self, num, model):\n",
    "        n_agents = len(self.agents)\n",
    "        entities = []\n",
    "        for i in range(n_agents, n_agents + num):\n",
    "            eid = 'Agent_%d' % i\n",
    "            self.agents.append(eid)\n",
    "            entities.append({'eid': eid, 'type': model})\n",
    "\n",
    "        return entities\n",
    "\n",
    "    def step(self, time, inputs, max_advance):\n",
    "        self.time = time\n",
    "        data = {}\n",
    "        for agent_eid, attrs in inputs.items():\n",
    "            delta_dict = attrs.get('delta', {})\n",
    "            if len(delta_dict) > 0:\n",
    "                data[agent_eid] = {'delta': list(delta_dict.values())[0]}\n",
    "                continue\n",
    "\n",
    "            values_dict = attrs.get('val_in', {})\n",
    "            if len(values_dict) != 1:\n",
    "                raise RuntimeError('Only one ingoing connection allowed per '\n",
    "                                   'agent, but \"%s\" has %i.'\n",
    "                                   % (agent_eid, len(values_dict)))\n",
    "            value = list(values_dict.values())[0]\n",
    "\n",
    "            if value >= 3:\n",
    "                delta = -1\n",
    "            elif value <= -3:\n",
    "                delta = 1\n",
    "            else:\n",
    "                continue\n",
    "\n",
    "            data[agent_eid] = {'delta': delta}\n",
    "\n",
    "        self.data = data\n",
    "\n",
    "        return None\n",
    "\n",
    "    def get_data(self, outputs):\n",
    "        data = {}\n",
    "        for agent_eid, attrs in outputs.items():\n",
    "            for attr in attrs:\n",
    "                if attr != 'delta':\n",
    "                    raise ValueError('Unknown output attribute \"%s\"' % attr)\n",
    "                if agent_eid in self.data:\n",
    "                    data['time'] = self.time\n",
    "                    data.setdefault(agent_eid, {})[attr] = self.data[agent_eid][attr]\n",
    "\n",
    "        return data\n",
    "\n",
    "\n",
    "def main():\n",
    "    return mosaik_api.start_simulation(Controller())\n",
    "\n",
    "\n",
    "# if __name__ == '__main__':\n",
    "#     main()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6b7af183-bced-4ac9-89a1-aa085bda0ab9",
   "metadata": {},
   "source": [
    "Next, we’ll create a new [scenario](Scenario.ipynb) to test our controller."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
