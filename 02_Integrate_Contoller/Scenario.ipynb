{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "4356064b-c24f-4f09-ac7e-d5ce688d8ef5",
   "metadata": {},
   "source": [
    "## Integrating a control mechanism"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4aeae1cf-a45a-4368-b9ee-81f5ff86fe11",
   "metadata": {},
   "source": [
    "The scenario that we’re going to create in this part of the tutorial will be similar to the one we created before\n",
    "but incorporate the control mechanism that we just created."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7a000991-52d8-4a16-a356-4a45b13d4890",
   "metadata": {},
   "source": [
    "Again, we start by setting some configuration values and creating a simulation world:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ddae48a1-1031-4b77-9442-29ff7cce1394",
   "metadata": {},
   "outputs": [],
   "source": [
    "# demo_2.py\n",
    "import mosaik\n",
    "import mosaik.util\n",
    "import import_ipynb\n",
    "\n",
    "# Sim config. and other parameters\n",
    "SIM_CONFIG = {\n",
    "    'ExampleSim': {\n",
    "        'python': '_02_simulator_mosaik:ExampleSim',\n",
    "    },\n",
    "    'ExampleCtrl': {\n",
    "        'python': '_01_controller:Controller',\n",
    "    },\n",
    "    'Collector': {\n",
    "        'python':'_04_collector:Collector',\n",
    "    },\n",
    "}\n",
    "END = 10  # 10 seconds\n",
    "\n",
    "# Create World\n",
    "world = mosaik.World(SIM_CONFIG)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14b7e286-042b-4b0c-b3ae-1a5087b3eaa5",
   "metadata": {},
   "source": [
    "We added ExampleCtrl to the sim config and let it be executed in-process with mosaik.\n",
    "\n",
    "We can now start one instance of each simulator:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3bc660f5-681d-41bd-8001-fdaa35882f8d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Start simulators\n",
    "examplesim = world.start('ExampleSim', eid_prefix='Model_')\n",
    "examplectrl = world.start('ExampleCtrl')\n",
    "collector = world.start('Collector')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1da6442a-a859-4338-9a12-59267450810a",
   "metadata": {},
   "source": [
    "We’ll create three model instances, the same number of agents, and one database:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "736e054f-ec44-4541-bbc0-aab512b2621b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Instantiate models\n",
    "models = [examplesim.ExampleModel(init_val=i) for i in range(-2, 3, 2)]\n",
    "agents = examplectrl.Agent.create(len(models))\n",
    "monitor = collector.Monitor()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fa1b67bf-d22a-4390-a52d-fd513270880e",
   "metadata": {},
   "source": [
    "We use a **list comprehension** to create three model instances with individual initial values (-2, 0 and 2).\n",
    "For instantiating the same number of agent instances we use `create()` which does the same as a list comprehension\n",
    "but is a bit shorter.\n",
    "\n",
    "Finally, we establish pairwise bi-directional connections between the models and the agents:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "391e6f48-1cb1-4590-8c4c-8dcbaa105a77",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Connect entities\n",
    "for model, agent in zip(models, agents):\n",
    "    world.connect(model, agent, ('val', 'val_in'))\n",
    "    world.connect(agent, model, 'delta', weak=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3f0261cb-50a3-4d81-a277-6a2223698286",
   "metadata": {},
   "source": [
    "The important thing here is the `weak=True` argument that we pass to the second connection. This tells mosaik how to\n",
    "resolve the cyclic dependency, i.e. which simulator should be stepped first in case that both simulators have a\n",
    "scheduled step at the same time. (In our example this will not happen, as the agents are only stepped by the models’\n",
    "outputs.)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9ac06c82-6583-4d2c-ae13-10dfa8c9edbd",
   "metadata": {},
   "source": [
    "Now we can connect the models and the agents to the monitor:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fbfcf274-bc12-4fd4-b719-e57970eef992",
   "metadata": {},
   "outputs": [],
   "source": [
    "mosaik.util.connect_many_to_one(world, models, monitor, 'val', 'delta')\n",
    "mosaik.util.connect_many_to_one(world, agents, monitor, 'delta')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "42f5b337",
   "metadata": {},
   "source": [
    "After we connected the simulators, the scenario looks like shown in this figure and can be executed:\n",
    "<div>\n",
    "<img src=\"scenario.png\" width=\"600\"/>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e776834c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run simulation\n",
    "world.run(until=END)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "29c92a69-c3a5-48e3-9ecc-e8bdd3552a07",
   "metadata": {},
   "source": [
    "In the printed output of the collector, you can see two important things: The first is that the agents only provide\n",
    "output when the delta of the controlled model is to be changed. And second, that the new delta is set at the models’\n",
    "subsequent step after it has been derived by the agents."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3314db3b-97bb-414d-a08d-52dd7d5396b6",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "If you are interested in more tutorials, you can have a look in adding a\n",
    "[master controller](../03_Same_Time_Loop/_01_controller_master.ipynb), which uses same time loops."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "mosaik",
   "language": "python",
   "name": "mosaik"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
