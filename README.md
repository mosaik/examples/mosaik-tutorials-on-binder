[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mosaik%2Fexamples%2Fmosaik-tutorials-on-binder/develop)

Click on the above image to access the demo tutorials on mybinder :)

The primary purpose of this tutorial is to give the user an interactive experience of using and learning mosaik with the help of Jupyter Notebooks. You don't have to delve into the intricacies of installing mosaik and the related dependencies. Just click on the above image to start interactively learning mosaik with the help of the designed tutorials. 

Note: mybinder might not build successfully in the first attempt. If that is the case, refresh the page or open the same link on a seperate window. 

Happy Learning! ;)
