{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "a7f2f2ba-29ea-4775-ab5c-3ba7550490ec",
   "metadata": {},
   "source": [
    "## Same-time loops"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "414bde29-ff3e-48e1-81b0-88942b478f54",
   "metadata": {},
   "source": [
    "Important use cases for same-time loops can be the initialization of simulation and communication between controllers or agents. As the scenario definition has to provide initialization values for cyclic data-flows and every cyclic data-flow will lead to an incrementing simulation time, it may take some simulation steps until all simulation components are in a stable state, especially, for simulations consisting of multiple physical systems. The communication between controllers or agents usually takes place at a different time scale than the simulation of the technical systems. Thus, same-time loops can be helpful to model this behavior in a realistic way.\n",
    "\n",
    "To give an example of same-time loops in mosaik, the previously shown scenario is extended with a master controller, which takes control over the other controllers. The communication between these two layers of controllers will take place in the same step without incrementing the simulation time. The code of the previous scenario is used as a base and extended as shown in the following."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bf252576-8d48-494d-856d-58c1bd6b1226",
   "metadata": {},
   "source": [
    "## Master Controller"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "78559242-64db-4f22-aebe-6a2a3c125f38",
   "metadata": {},
   "source": [
    "The master controller bases on the code of the controller of the previous scenario. The first small change for the master controller is in the meta data dictionary, where new attribute names are defined. The ‘delta_in’ represent the delta values of the controllers, which will be limited by the master controller. The results of this control function will be returned to the controllers as ‘delta_out’."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "befdbe5c-a0e0-41b4-bb65-9795c67a99df",
   "metadata": {},
   "outputs": [],
   "source": [
    "import mosaik_api\n",
    "\n",
    "META = {\n",
    "    'type': 'event-based',\n",
    "    'models': {\n",
    "        'Agent': {\n",
    "            'public': True,\n",
    "            'params': [],\n",
    "            'attrs': ['delta_in', 'delta_out'],\n",
    "        },\n",
    "    },\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fe19258b-bd1b-4845-be13-7d102434b95a",
   "metadata": {},
   "source": [
    "The `__init__()` is extended with **self.cache** for storing the inputs and **self.time** for storing the current simulation time, which is initialized with 0."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1f3c3e6f-fc79-44d2-ad1a-d800b73f468e",
   "metadata": {},
   "outputs": [],
   "source": [
    "class Controller(mosaik_api.Simulator):\n",
    "    def __init__(self):\n",
    "        super().__init__(META)\n",
    "        self.agents = []\n",
    "        self.data = {}\n",
    "        self.cache = {}\n",
    "        self.time = 0"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "81d35268-e650-44cb-bdb0-8268bed0079c",
   "metadata": {},
   "source": [
    "The `step()` is changed, so that first the current time is updated in the **self.time** variable. Also the control function is changed. The master controller gets the delta output of the other controllers as ‘delta_in’ and stores the last value of each controller in the **self.cache**. This is needed, because the controllers are event-based and the current values are only sent if the values changes. The control function of the master controller limits the sum of all deltas to be **< 1** and **> -1**. If these limits are exceeded the delta of all controllers will be overwritten by the master controller with **0** and sent to the other controller as ‘delta_out’."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4b4e26b0-ef90-4d36-a028-4c76e3e49500",
   "metadata": {},
   "outputs": [],
   "source": [
    "    def step(self, time, inputs, max_advance):\n",
    "        self.time = time\n",
    "        data = {}\n",
    "        for agent_eid, attrs in inputs.items():\n",
    "            values_dict = attrs.get('delta_in', {})\n",
    "            for key, value in values_dict.items():\n",
    "                self.cache[key] = value\n",
    "        \n",
    "        if sum(self.cache.values()) < -1:\n",
    "            data[agent_eid] = {'delta_out': 0}\n",
    "\n",
    "        self.data = data\n",
    "\n",
    "        return None"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f04e845f-bcfb-4749-9bd4-25b08931d220",
   "metadata": {},
   "source": [
    "Additionally, two small changes in the `get_data()` method were done. First, the name was updated to ‘delta_out’ in the check for the correct attribute name. Second, the current time, which was stored previously in the `step()`, is added to the output cache dictionary. This informs mosaik that the simulation should start or stay in a same-time loop if also output data for ‘delta_out’ is provided."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "42a4e617-103e-4387-8537-ca0b2041e33a",
   "metadata": {},
   "outputs": [],
   "source": [
    "    def get_data(self, outputs):\n",
    "        data = {}\n",
    "        for agent_eid, attrs in outputs.items():\n",
    "            for attr in attrs:\n",
    "                if attr != 'delta_out':\n",
    "                    raise ValueError('Unknown output attribute \"%s\"' % attr)\n",
    "                if agent_eid in self.data:\n",
    "                    data['time'] = self.time\n",
    "                    data.setdefault(agent_eid, {})[attr] = self.data[agent_eid][attr]\n",
    "\n",
    "        return data\n"
   ]
  },
  {
   "cell_type": "markdown",
   "source": [
    "For the integration of the master controller in a scenario, also some small changes have to be made to the [controller](_02_controller_demo_3.ipynb) from part 1."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "id": "9d3a8983-4c0a-4647-bb9e-5806e1d80af8",
   "metadata": {},
   "source": [
    "## Complete code for controller is as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4f60d178-4da2-4acb-ae66-f3a6461a1c39",
   "metadata": {},
   "outputs": [],
   "source": [
    "import mosaik_api\n",
    "\n",
    "\n",
    "META = {\n",
    "    'type': 'event-based',\n",
    "    'models': {\n",
    "        'Agent': {\n",
    "            'public': True,\n",
    "            'params': [],\n",
    "            'attrs': ['delta_in', 'delta_out'],\n",
    "        },\n",
    "    },\n",
    "}\n",
    "\n",
    "\n",
    "class Controller(mosaik_api.Simulator):\n",
    "    def __init__(self):\n",
    "        super().__init__(META)\n",
    "        self.agents = []\n",
    "        self.data = {}\n",
    "        self.cache = {}\n",
    "        self.time = 0\n",
    "\n",
    "    def create(self, num, model):\n",
    "        n_agents = len(self.agents)\n",
    "        entities = []\n",
    "        for i in range(n_agents, n_agents + num):\n",
    "            eid = 'Master_Agent_%d' % i\n",
    "            self.agents.append(eid)\n",
    "            entities.append({'eid': eid, 'type': model})\n",
    "\n",
    "        return entities\n",
    "\n",
    "    def step(self, time, inputs, max_advance):\n",
    "        self.time = time\n",
    "        data = {}\n",
    "        for agent_eid, attrs in inputs.items():\n",
    "            values_dict = attrs.get('delta_in', {})\n",
    "            for key, value in values_dict.items():\n",
    "                self.cache[key] = value\n",
    "        \n",
    "        if sum(self.cache.values()) < -1:\n",
    "            data[agent_eid] = {'delta_out': 0}\n",
    "\n",
    "        self.data = data\n",
    "\n",
    "        return None\n",
    "\n",
    "    def get_data(self, outputs):\n",
    "        data = {}\n",
    "        for agent_eid, attrs in outputs.items():\n",
    "            for attr in attrs:\n",
    "                if attr != 'delta_out':\n",
    "                    raise ValueError('Unknown output attribute \"%s\"' % attr)\n",
    "                if agent_eid in self.data:\n",
    "                    data['time'] = self.time\n",
    "                    data.setdefault(agent_eid, {})[attr] = self.data[agent_eid][attr]\n",
    "\n",
    "        return data\n",
    "\n",
    "\n",
    "def main():\n",
    "    return mosaik_api.start_simulation(Controller())\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
