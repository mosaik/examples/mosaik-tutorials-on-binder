{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "2c78739c-420a-4340-ba80-febeee28401e",
   "metadata": {},
   "source": [
    "## Creating and running simple simulation scenarios\n",
    "We will now create a simple scenario with mosaik in which we use a simple data collector to print some output from our simulation. That means, we will instantiate a few ExampleModels and a data monitor. We will then connect the model instances to that monitor and simulate that for some time."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18b48ac3-f4c3-4a6d-96a1-b1583fc99feb",
   "metadata": {},
   "source": [
    "## Configuration \n",
    "You should define the most important configuration values for your simulation as “constants” on top of your scenario file. This makes it easier to see what’s going on and change the parameter values.\n",
    "\n",
    "Two of the most important parameters that you need in almost every simulation are the simulator configuration and the duration of your simulation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "06ceff8d-08fd-490b-a541-15be35998045",
   "metadata": {},
   "outputs": [],
   "source": [
    "import mosaik\n",
    "import mosaik.util\n",
    "import import_ipynb\n",
    "\n",
    "# Sim config. and other parameters\n",
    "SIM_CONFIG = {\n",
    "    'ExampleSim': {\n",
    "        'python': '_02_simulator_mosaik:ExampleSim',\n",
    "    },\n",
    "    'Collector': {\n",
    "        'python': '_03_collector:Collector',\n",
    "    },\n",
    "}\n",
    "END = 10  # 10 seconds"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eb179b23-de82-485a-80ec-4f967d532e5b",
   "metadata": {},
   "source": [
    "The sim config specifies which simulators are available and how to start them. In the example above, we list our ExampleSim as well as Collector (the names are arbitrarily chosen). For each simulator listed, we also specify how to start it.\n",
    "\n",
    "Since our example simulator is, like mosaik, written in Python 3, mosaik can just import it and execute it in-process. The line `'python': '_02_simulator_mosaik:ExampleSim'` tells mosaik to import the package `_02_simulator_mosaik` and instantiate the class `ExampleSim` from it.\n",
    "\n",
    "The [data collector](01_Integrate_Model/_03_collector.ipynb) will be started as external process which will communicate with mosaik via sockets. The line `'cmd': '%(python)s _03_collector.py %(addr)s'` tells mosaik to start the simulator by executing the command `python _03_collector.py.` Beforehand, mosaik replaces the placeholder `%(python)s` with the current python interpreter (the same as used to execute the scenario script) and `%(addr)s` with its actual socket address HOSTNAME:PORT so that the simulator knows where to connect to."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5aeeee79-8168-460b-92ea-1a0ba3460a29",
   "metadata": {},
   "source": [
    "## The world\n",
    "The next thing we do is instantiating a `World` object. This object will hold all simulation state. It knows which simulators are available and started, which entities exist and how they are connected. It also provides most of the functionality that you need for modelling your scenario:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4919fe38-0f85-42c5-9a4f-8e73b8c0e769",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create World\n",
    "world = mosaik.World(SIM_CONFIG)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ada014b1-b245-44f2-aab4-797f4bffeeb1",
   "metadata": {},
   "source": [
    "## The Scenario\n",
    "Before we can instantiate any simulation models, we first need to start the respective simulators. This can be done by calling World.start(). It takes the name of the simulator to start and, optionally, some simulator parameters which will be passed to the simulators `init()` method. So lets start the example simulator and the data collector:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f15265b7-d4e1-48f5-9afa-554e3fff9b7d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Start simulators\n",
    "examplesim = world.start('ExampleSim', eid_prefix='Model_')\n",
    "collector = world.start('Collector')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ae3373ca-06a9-4c3c-a850-55f50e7a9219",
   "metadata": {},
   "source": [
    "We also set the eid_prefix for our example simulator. What gets returned by `World.start()` is called a model factory.\n",
    "\n",
    "We can use this factory object to create model instances within the respective simulator. In your scenario, such an instance is represented as an `Entity`. The model factory presents the available models as if they were classes within the factory’s namespace. So this is how we can create one instance of our example model and one ‘Monitor’ instance:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7545b237-18c1-438b-b688-141b00e3dc63",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Instantiate models\n",
    "model = examplesim.ExampleModel(init_val=2)\n",
    "monitor = collector.Monitor()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0c9cf108-0a7a-467f-aa82-4b83b27ba395",
   "metadata": {},
   "source": [
    "The init_val parameter that we passed to `ExampleModel` is the same as in the `create()` method of our Sim API implementation.\n",
    "\n",
    "Now, we need to connect the example model to the monitor. That’s how we tell mosaik to send the outputs of the example model to the monitor."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "80d2ac5b-a165-423d-b2e7-4fbf5438abdf",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Connect entities\n",
    "world.connect(model, monitor, 'val', 'delta')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f2be2390-aa11-407e-9830-c2d3589571e2",
   "metadata": {},
   "source": [
    "The method `World.connect()` takes one entity pair – the source and the destination entity, as well as a list of attributes or attribute tuples. If you only provide single attribute names, mosaik assumes that the source and destination use the same attribute name. If they differ, you can instead pass a tuple like `('val_out', 'val_in')`.\n",
    "\n",
    "Quite often, you will neither create single entities nor connect single entity pairs, but work with large(r) sets of entities. Mosaik allows you to easily create multiple entities with the same parameters at once. It also provides some utility functions for connecting sets of entities with each other. So lets create two more entities and connect them to our monitor:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b59a0d00-fb9f-4b12-b3a5-d4269ae1ad6b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create more entities\n",
    "more_models = examplesim.ExampleModel.create(2, init_val=3)\n",
    "mosaik.util.connect_many_to_one(world, more_models, monitor, 'val', 'delta')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "165a7c28-10bc-4cf0-bf39-c6492bcf76cd",
   "metadata": {},
   "source": [
    "Instead of instantiating the example model directly, we called its static method `create()` and passed the number of instances to it. It returns a list of entities (two in this case). We used the utility function `mosaik.util.connect_many_to_one()` to connect all of them to the database. This function has a similar signature as `World.connect()`, but the first two parameters are a world instance and a set (or list) of entities that are all connected to the dest_entity.\n",
    "\n",
    "Mosaik also provides the function `mosaik.util.connect_randomly()`. This method randomly connects one set of entities to another set. These two methods should cover most use cases. For more special ones, you can implement custom functions based on the primitive `World.connect()`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e008a0df-2a62-45d9-919d-7b2282701ecc",
   "metadata": {},
   "source": [
    "## The Simulation\n",
    "After we started, initialized, and connected the simulators, the scenario looks like shown in this figure:\n",
    "<div>\n",
    "<img src=\"scenario.png\" width=\"600\"/>\n",
    "</div>\n",
    "In order to start the simulation, we call `World.run()` and specify for how long we want our simulation to run and get the following output:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fa00ee91-c509-4868-91ae-11363502752e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run simulation\n",
    "world.run(until=END)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4ea6918d-acc8-4aab-857a-90cdfdab7637",
   "metadata": {},
   "source": [
    "This was the first part of the mosaik tutorial.\n",
    "In the second part, a [control mechanism](../02_Integrate_Contoller/_01_controller.ipynb) is added to the scenario described here."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "mosaik",
   "language": "python",
   "name": "mosaik"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
